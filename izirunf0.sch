EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "IZIRUNF0"
Date "2020-07-06"
Rev "A"
Comp "IZITRON"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4910 1515 1710 3830
U 5EB5BEF3
F0 "mcu" 118
F1 "mcu.sch" 79
F2 "BOOT" I R 6620 1700 50 
F3 "NRST" I R 6620 1600 50 
F4 "PA0_WKUP" I R 6620 1800 50 
F5 "PA11_I2C2_SCL" O L 4910 1800 50 
F6 "PA12_I2C2_SDA" B L 4910 1950 50 
F7 "PB13_SPI2_SCK" O L 4910 2300 50 
F8 "PB14_SPI2_MISO" I L 4910 2600 50 
F9 "PB15_SPI2_MOSI" O L 4910 2450 50 
F10 "PB12_SPI2_CS" O L 4910 2750 50 
F11 "PA9_UART1_TX" O R 6620 2000 50 
F12 "PA10_UART1_RX" I R 6620 2100 50 
F13 "PB6_I2C1_SCL" O R 6620 2300 50 
F14 "PB7_I2C1_SDA" B R 6620 2400 50 
F15 "PA5_SPI1_SCK" O R 6620 2600 50 
F16 "PA7_SPI1_MOSI" O R 6620 2800 50 
F17 "PC13_SPI1_CS0" O R 6620 2900 50 
F18 "PA15_SPI1_CS1" O R 6620 3000 50 
F19 "PB3_SPI1_CS2" O R 6620 3100 50 
F20 "PA1_ADC_IN1" I R 6620 3300 50 
F21 "PA2_ADC_IN2" I R 6620 3400 50 
F22 "PB0_TIM3_CH3" O R 6620 3800 50 
F23 "PB1_TIM3_CH4" O R 6620 3900 50 
F24 "PB4_TIM3_CH1" O R 6620 3600 50 
F25 "PB5_TIM3_CH2" O R 6620 3700 50 
F26 "PB8_TIM16_CH1" O R 6620 4000 50 
F27 "PB9_TIM17_CH1" O R 6620 4100 50 
F28 "PA3_IO0" B R 6620 4255 50 
F29 "PA13_SYS_SWDIO" B R 6620 5000 50 
F30 "PA4_IO1" B R 6620 4355 50 
F31 "PA14_SYS_SWCLK" I R 6620 4900 50 
F32 "PA6_SPI1_MISO" I R 6620 2700 50 
F33 "PB10_IO2" B R 6620 4450 50 
F34 "PB11_IO3" B R 6620 4550 50 
$EndSheet
$Sheet
S 2055 4175 1795 1170
U 5F047122
F0 "power" 118
F1 "power.sch" 79
$EndSheet
$Sheet
S 2060 1510 1785 1630
U 5F0797FF
F0 "memory" 118
F1 "memory.sch" 79
F2 "FLASH_CLK" I R 3845 2300 50 
F3 "FLASH_MOSI" I R 3845 2450 50 
F4 "FLASH_MISO" O R 3845 2600 50 
F5 "~FLASH_CS" I R 3845 2750 50 
F6 "EEPROM_SDA" B R 3845 1950 50 
F7 "EEPROM_SCL" I R 3845 1800 50 
$EndSheet
$Sheet
S 7485 1515 1770 3835
U 5EF6AD0D
F0 "connector" 118
F1 "connector.sch" 79
F2 "USART1_TX" I L 7485 2000 50 
F3 "USART1_RX" O L 7485 2100 50 
F4 "I2C1_SCL" I L 7485 2300 50 
F5 "I2C1_SDA" B L 7485 2400 50 
F6 "SPI1_SCK" I L 7485 2600 50 
F7 "SPI1_MISO" O L 7485 2700 50 
F8 "SPI1_MOSI" I L 7485 2800 50 
F9 "SPI1_CS0" I L 7485 2900 50 
F10 "SPI1_CS1" I L 7485 3000 50 
F11 "SPI1_CS2" I L 7485 3100 50 
F12 "ADC0" O L 7485 3300 50 
F13 "ADC1" O L 7485 3400 50 
F14 "PWM0" I L 7485 3600 50 
F15 "PWM1" I L 7485 3700 50 
F16 "PWM2" I L 7485 3800 50 
F17 "PWM3" I L 7485 3900 50 
F18 "PWM4" I L 7485 4000 50 
F19 "PWM5" I L 7485 4100 50 
F20 "IO0" B L 7485 4255 50 
F21 "IO1" B L 7485 4355 50 
F22 "NRST" I L 7485 1600 50 
F23 "BOOT" I L 7485 1700 50 
F24 "WKUP" I L 7485 1800 50 
F25 "SWD_SWCLK" O L 7485 4900 50 
F26 "SWD_SWDIO" B L 7485 5000 50 
F27 "IO2" B L 7485 4450 50 
F28 "IO3" B L 7485 4550 50 
$EndSheet
Wire Wire Line
	3845 1800 4910 1800
Wire Wire Line
	4910 1950 3845 1950
Wire Wire Line
	4910 2450 3845 2450
Wire Wire Line
	3845 2600 4910 2600
Wire Wire Line
	4910 2750 3845 2750
Wire Wire Line
	6620 1600 7485 1600
Wire Wire Line
	7485 1700 6620 1700
Wire Wire Line
	6620 1800 7485 1800
Wire Wire Line
	7485 2000 6620 2000
Wire Wire Line
	6620 2100 7485 2100
Wire Wire Line
	7485 2300 6620 2300
Wire Wire Line
	6620 2400 7485 2400
Wire Wire Line
	7485 2600 6620 2600
Wire Wire Line
	6620 2700 7485 2700
Wire Wire Line
	7485 2800 6620 2800
Wire Wire Line
	6620 2900 7485 2900
Wire Wire Line
	7485 3000 6620 3000
Wire Wire Line
	6620 3100 7485 3100
Wire Wire Line
	7485 3300 6620 3300
Wire Wire Line
	6620 3400 7485 3400
Wire Wire Line
	7485 3600 6620 3600
Wire Wire Line
	6620 3700 7485 3700
Wire Wire Line
	7485 3800 6620 3800
Wire Wire Line
	6620 3900 7485 3900
Wire Wire Line
	7485 4000 6620 4000
Wire Wire Line
	6620 4100 7485 4100
Wire Wire Line
	7485 4255 6620 4255
Wire Wire Line
	6620 4355 7485 4355
Wire Wire Line
	7485 4900 6620 4900
Wire Wire Line
	6620 5000 7485 5000
Wire Wire Line
	7485 4450 6620 4450
Wire Wire Line
	7485 4550 6620 4550
Wire Wire Line
	4910 2300 3845 2300
$EndSCHEMATC
